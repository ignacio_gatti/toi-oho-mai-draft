﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Toi_Oho_Mai_Draft
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            var listShow = new List<myListView>
            {
                new myListView {topic = "Faculty", info="Contact the people in charge" },
                new myListView {topic="Bus Routes", info="See the best bus routes from your home to get to school" },
                new myListView {topic="Courses", info="Decide which career you'll follow" }
            };

            var settingsShow = new List<mySettingsView>
            {
                new mySettingsView { topic = "Account", info="Change Your Account Details", image="http://shushi168.com/data/out/190/37389918-profile-pics.png" },
                new mySettingsView { topic = "Language", info="Change language settings", image="https://flan.csusb.edu/images/world.png" },
                new mySettingsView { topic = "Your Grades", info="See what you got in current/previous courses", image="http://www.bamradionetwork.com/images/A.jpg" }

            };

            myListShow.ItemsSource = listShow;
            mySettingsList.ItemsSource = settingsShow;

        }

        public string status2 = "closed";
        public string status3 = "closed";


        //********************************HOME BUTTON****************************
        void changeGridView1(Object sender, EventArgs e)
        {

            firstGrid.Width = new GridLength(1, GridUnitType.Star);
            secondGrid.Width = new GridLength(0, GridUnitType.Star);
            thirdGrid.Width = new GridLength(0, GridUnitType.Star);
            status2 = "closed";
            status3 = "closed";
        }

        //********************************OPTIONS BUTTON****************************
        void changeGridView2(Object sender, EventArgs e)
        {
            if (status2 == "closed")
            {

                secondGrid.Width = new GridLength(1, GridUnitType.Star);
                firstGrid.Width = new GridLength(0, GridUnitType.Star);
                thirdGrid.Width = new GridLength(0, GridUnitType.Star);
                status2 = "opened";
                status3 = "closed";
            }

            else if (status2 == "opened")
            {
                firstGrid.Width = new GridLength(1, GridUnitType.Star);
                secondGrid.Width = new GridLength(0, GridUnitType.Star);
                thirdGrid.Width = new GridLength(0, GridUnitType.Star);
                status2 = "closed";
            }


        }

        //********************************SETTINGS BUTTON****************************
        void changeGridView3(Object sender, EventArgs e)
        {
            if (status3 == "closed")
            {

                thirdGrid.Width = new GridLength(1, GridUnitType.Star);
                firstGrid.Width = new GridLength(0, GridUnitType.Star);
                secondGrid.Width = new GridLength(0, GridUnitType.Star);
                status3 = "opened";
                status2 = "closed";
            }

            else if (status3 == "opened")
            {
                firstGrid.Width = new GridLength(1, GridUnitType.Star);
                thirdGrid.Width = new GridLength(0, GridUnitType.Star);
                secondGrid.Width = new GridLength(0, GridUnitType.Star);
                status3 = "closed";
            }


        }

        public class myListView
        {
            public string topic { get; set; }

            public string info { get; set; }

            //public string  { get; set; }
        }

        public class mySettingsView
        {
            public string topic { get; set; }

            public string info { get; set; }

            public string image { get; set; }
        }
    }
}
